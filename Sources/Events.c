/* ###################################################################
**     Filename    : Events.c
**     Project     : ADC_DAC
**     Processor   : MK64FN1M0VLL12
**     Component   : Events
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-09-10, 16:06, # CodeGen: 0
**     Abstract    :
**         This is user's event module.
**         Put your event handler code here.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file Events.c
** @version 01.00
** @brief
**         This is user's event module.
**         Put your event handler code here.
*/         
/*!
**  @addtogroup Events_module Events module documentation
**  @{
*/         
/* MODULE Events */

#include "Cpu.h"
#include "Events.h"

#ifdef __cplusplus
extern "C" {
#endif 

#define tam_buffer 512
#define blockSize    1

static int flag_periodo = 0;
int wr_pointer = 0;
q15_t buffer_in[ tam_buffer ];
q15_t buffer_out[ tam_buffer ];
q15_t state_buffer [ blockSize + N_WINDOW - 1 ];
static uint16_t resultado_adc;
static int filter_mode = 0;

arm_fir_instance_q15 filter;

/* User includes (#include below this line is not maintained by Processor Expert) */

/*
** ===================================================================
**     Callback    : hwTim1_OnTimeOut
**     Description : This callback is called when the timer expires.
**     Parameters  :
**       data - User parameter for the callback function.
**     Returns : Nothing
** ===================================================================
*/

  void captura_ADC()
  {
    ADC16_DRV_ConfigConvChn(adConv1_IDX,0,&adConv1_ChnConfig0);
    ADC16_DRV_WaitConvDone(adConv1_IDX,0);
    resultado_adc = ADC16_DRV_GetConvValueRAW(adConv1_IDX,0);
    ADC16_DRV_PauseConv(adConv1_IDX,0);

    *(buffer_in+wr_pointer) = resultado_adc;

    if (filter_mode != 0)
      {
        arm_fir_q15( &filter, ( buffer_in + wr_pointer ), ( buffer_out + wr_pointer ), blockSize );
      }
    else // bypass
      {
        *(buffer_out+wr_pointer) = resultado_adc;
      }

    DAC_DRV_Output(daConv1_IDX,(uint16_t)*(buffer_out + wr_pointer) >> 4);
    wr_pointer = (wr_pointer + 1) % tam_buffer;
  }


  void hwTim1_OnTimeOut(void* data)
  {
    /* Write your code here ... */
    switch(flag_periodo)
      {
      case 1:
        GPIO_DRV_ClearPinOutput(RED_LED);
        GPIO_DRV_SetPinOutput(GREEN_LED);
        GPIO_DRV_SetPinOutput(BLUE_LED);
        captura_ADC();
        break;
      case 2:
        GPIO_DRV_SetPinOutput(RED_LED);
        GPIO_DRV_ClearPinOutput(GREEN_LED);
        GPIO_DRV_SetPinOutput(BLUE_LED);
        captura_ADC();
        break;
      case 3:
        GPIO_DRV_SetPinOutput(RED_LED);
        GPIO_DRV_SetPinOutput(GREEN_LED);
        GPIO_DRV_ClearPinOutput(BLUE_LED);
        captura_ADC();
        break;
      case 4:
        GPIO_DRV_ClearPinOutput(RED_LED);
        GPIO_DRV_ClearPinOutput(GREEN_LED);
        GPIO_DRV_SetPinOutput(BLUE_LED);
        captura_ADC();
        break;
      case 5:
        GPIO_DRV_SetPinOutput(RED_LED);
        GPIO_DRV_ClearPinOutput(GREEN_LED);
        GPIO_DRV_ClearPinOutput(BLUE_LED);
        captura_ADC();
        break;
      case 0:
        GPIO_DRV_ClearPinOutput(RED_LED);
        GPIO_DRV_SetPinOutput(GREEN_LED);
        GPIO_DRV_ClearPinOutput(BLUE_LED);
        captura_ADC();
        break;
      }
  }

/*
** ===================================================================
**     Interrupt handler : PORTA_IRQHandler
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
  void PORTA_IRQHandler(void)
  {
    /* Clear interrupt flag.*/
    PORT_HAL_ClearPortIntFlag(PORTA_BASE_PTR);
    /* Write your code here ... */
    switch(flag_periodo)
      {
      case 0:
        HWTIMER_SYS_SetPeriod(&hwTim1_Handle,125); // 8kHz ROJO
        switch (filter_mode)
        {
        	case 1:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_lp_8_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 2:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_hp_8_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 3:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_bp_8_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 4:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_rb_8_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	default:
        		break;
        }
        flag_periodo++;
        break;
      case 1:
        HWTIMER_SYS_SetPeriod(&hwTim1_Handle,63); // ~16kHz VERDE
        switch (filter_mode)
        {
        	case 1:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_lp_16_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 2:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_hp_16_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 3:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_bp_16_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 4:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_rb_16_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	default:
        		break;
        }
        flag_periodo++;
        break;
      case 2:
        HWTIMER_SYS_SetPeriod(&hwTim1_Handle,42); // ~24kHz AZUL
        switch (filter_mode)
        {
        	case 1:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_lp_24_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 2:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_hp_24_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 3:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_bp_24_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 4:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_rb_24_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	default:
        		break;
        }
        flag_periodo++;
        break;
      case 3:
        HWTIMER_SYS_SetPeriod(&hwTim1_Handle,31); // ~32kHz AMARILLO
        switch (filter_mode)
        {
        	case 1:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_lp_32_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 2:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_hp_32_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 3:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_bp_32_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 4:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_rb_32_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	default:
        		break;
        }
        flag_periodo++;
        break;
      case 4:
        HWTIMER_SYS_SetPeriod(&hwTim1_Handle,23); // ~44kHz CELESTE
        switch (filter_mode)
        {
        	case 1:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_lp_44_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 2:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_hp_44_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 3:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_bp_44_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 4:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_rb_44_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	default:
        		break;
        }
        flag_periodo++;
        break;
      case 5:
        HWTIMER_SYS_SetPeriod(&hwTim1_Handle,21); // ~48kHz VIOLETA
        switch (filter_mode)
        {
        	case 1:
        		arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_array_lp_48[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 2:
        		arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_array_hp_48[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 3:
        		arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_array_bp_48[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	case 4:
        		//arm_fir_init_q15(&filter, N_WINDOW, (q15_t *)&coeff_rb_48_array[0], (q15_t *) &state_buffer[0], blockSize);
        		break;
        	default:
        		break;
        }
        flag_periodo = 0;
        break;
      }
  }

/*
** ===================================================================
**     Interrupt handler : PORTC_IRQHandler
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
  void PORTC_IRQHandler(void)
  {
    /* Clear interrupt flag.*/
    PORT_HAL_ClearPortIntFlag(PORTC_BASE_PTR);
    /* Write your code here ... */
    filter_mode = (filter_mode+1)%5;
  }

/* END Events */

#ifdef __cplusplus
}  /* extern "C" */
#endif 

/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.5 [05.21]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
